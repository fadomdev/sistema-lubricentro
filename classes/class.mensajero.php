<?php
include_once "class.conex.php";
include_once "class.perfil.php";

class Mensajero{

	private $db;
	private $salt;
	private $perfil;
	private $table;

	function Mensajero(){
		$this->db = new conexion();
		$this->perfil = new Perfil();
		$this->salt = "7p69tiDcjRKhYJlN1Wf48";	
		$this->table = "sgcp_mensajero";
	}
	
	function getAllUsuarios($page, $reg, $arr)
	{
		$offset = ($page-1)*$reg;
		
		$where = "";
		
		/*
		$where .= isset($arr['ft_nombre']) && $arr['ft_nombre'] != "" ? " AND usu_nombre LIKE '%".$this->db->db_real_escape_string($arr['ft_nombre'])."%'" : "";
		$where .= isset($arr['ft_email']) && $arr['ft_email'] != "" ? " AND usu_email LIKE '%".$this->db->db_real_escape_string($arr['ft_email'])."%'" : "";
		$where .= isset($arr['ft_estado']) && $arr['ft_estado'] != 2 ? " AND usu_activo = ".$arr['ft_estado']."" : "";
		$where .= isset($arr['ft_fecha_ing_in']) && $arr['ft_fecha_ing_in'] != "" ? " AND DATE_FORMAT(usu_fecha_ing,'%Y-%m-%d') >= '".date("Y-m-d",strtotime($arr['ft_fecha_ing_in']))."'" : "";
		$where .= isset($arr['ft_fecha_ing_fi']) && $arr['ft_fecha_ing_fi'] != "" ? " AND DATE_FORMAT(usu_fecha_ing,'%Y-%m-%d') <= '".date("Y-m-d",strtotime($arr['ft_fecha_ing_fi']))."'" : "";
		*/
		
		$qry = "SELECT * FROM ".$this->table." WHERE 1 ".$where;
		$res = $this->db->db_query($qry." LIMIT ".$offset.", ".$reg);
		$rows = array();
		$result['total'] = $this->db->db_numrows($this->db->db_query($qry));
		while($row = $this->db->db_fetch_object($res))
		{
			$row->usu_fecha_ing = $this->db->fecha($row->usu_fecha_ing,true,"output");
			$row->usu_perfil = $row->usu_perfil == null || $row->usu_perfil == '' || $row->usu_perfil == 0 ? 'No asignado' : $this->perfil->getNombrePerfil($row->usu_perfil);
			array_push($rows,$row);
		}		 
		$result['rows'] = $rows;
		echo json_encode($result);
	}
	
	public function agregarMensaje($arr){
		$qry = "";
		$qry .= " INSERT INTO ".$this->table." (men_titulo, men_subtitulo, men_url, men_mensaje, men_fecha_ing) ";
		$qry .= " VALUES ( ";
		$qry .= " '".$this->db->db_real_escape_string($arr['txtTitulo'])."', ";
		$qry .= " '".$this->db->db_real_escape_string($arr['txtSubTitulo'])."', ";
		$qry .= " '".$this->db->db_real_escape_string($arr['txtURL'])."', ";
		$qry .= " '".$this->db->db_real_escape_string($arr['txtMensaje'])."', ";
		$qry .= " NOW() ); ";
	
		if($this->db->db_query($qry)){
			echo ' var r = {"resultado": "OK", "mensaje": "El mensaje ha sido enviado correctamente"};';
		} else {
			echo ' var r = {"resultado": "FAIL", "mensaje": "El mensaje no pudo ser enviado, intenta nuevamente"};';
		}
	}
}

?>